import java.util.Random;
import java.util.Scanner;

public class Main {
   public static void main(String[] args) {
      Scanner scnr = new Scanner(System.in);
      Random randGen = new Random();
      int hold = 0;//number of dice not being rerolled
      int rounds = 2;//number of rerolls allowed


      //generate the initial dice roll - save it in an int[5]

      System.out.println("The dice fall:");
      //print(printDice(dice))

      //determine which dice to hold holding(scnr)
      System.out.println("You chose to reroll "+(5-hold)+" dice.");
      while(hold<5&&rounds>0){
        rounds--;
			//ask and reroll some of the dice rerollDice(dice, scnr, randGen, hold)

      if(rounds>0){//only ask if there are rerolls left
         System.out.println("The dice fall:");
		   //printDice(dice)
   		System.out.print("How many dice would you like to hold? ");
   		hold = scnr.nextInt();
         System.out.println();//newline
         }
      }//end re-roll loop

      //final dice display
      System.out.println("The final dice are:");
      //print(printDice(dice))

      //Display score
      System.out.println("Score:");
      //print(printScore(dice))
      scnr.close();

   }//end main method

//int holding(Scanner scnr)
  /*This method asks how many of the 5 dice the palyer would like to keep (ie, not reroll)
  After getting the input, check to see if it was a valid number of dice and ask the player again if they did not put in a valid number of dice.
  Return the number of dice the player is holding.
  */

//int[] rerollDice(int[] dice, Scanner scnr, Random randGen, int hold)
  /*for the number of dice the player is not holding ask which die they would like to reroll and validate that is is a usable die number (1-5, dice there are 5 dice). generate a new random number for the selected die. Return the new dice array.*/


//String printDice(int[] dice)
   /*This methos creates a string representation of the dice array seperated by " "
   ie. rep = "1 2 3...", full credit will be given with or without a trailing " "
   */

//int[] kinds(int[] dice) method
   /*this method counts the number of unique values and stores them in an int array
   ie 1 3 3 4 6 yields [1,0,2,1,0,1]
   */

//String printScore(int[]dice) method
   /*this methiod returns a string representation of the different winning values. The test for this method is worth 7 points.
   possible return values
	      rep = "Yahtzee"; all dice the same value
	      rep = "Large Straight"; all dice in sequential order
	      rep = "Small Straight"; 4 dice in sequential order
	      rep = "Four of a Kind"; 4 dice with the same value
	      rep = "Full House"; 3 dice with the same value and another 2 dice with the same value
	      rep = "Three of a Kind"; 3 dice with the same value
	      rep = "You did not score anything noteworthy."; anything else
   */

}//end class Main