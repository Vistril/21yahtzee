import org.junit.Test;
import static org.junit.Assert.*;
import java.util.Scanner;
import java.util.Random;

public class Tests {
  Yahtzee student = new Yahtzee();

  //Test holding()
  @Test
  public void testHolding1(){
    String input = "30 4";
    Scanner s = new Scanner(input);
    int c = 4;
    System.out.println("---Captured Output for testHolding1()---");
    int r = student.holding(s);
    System.out.println("\n---End---");
    assertEquals(c,r);
  }
  @Test
  public void testHolding2(){
    String input = "0";
    Scanner s = new Scanner(input);
    int c = 0;
    System.out.println("---Captured Output for testHolding2()---");
    int r = student.holding(s);
    System.out.println("\n---End---");
    System.out.println();
    assertEquals(c,r);
  }
  //Test rerollDice()
  @Test
  public void testRerollDice1(){
    String input = " 14 1 ";
    Scanner s = new Scanner(input);
    Random rand = new Random();
    int[] start = {1,2,3,4,5};
    rand.setSeed(0);
    int[] c = {1,2,3,4,5};
    System.out.println("---Captured Output for testRerollDice1()---");
    int[] r = student.rerollDice(start,s,rand,4);
    System.out.println("\n---End---");
    assertArrayEquals(c,r);
  }
  @Test
  public void testRerollDice2(){
    String input = " 14 1 3 5 ";
    Scanner s = new Scanner(input);
    Random rand = new Random();
    int[] start = {1,2,3,4,5};
    rand.setSeed(25);
    int[] c = {6,2,1,4,6};
    System.out.println("---Captured Output for testRerollDice2()---");
    int[] r = student.rerollDice(start,s,rand,2);
    System.out.println("\n---End---");
    assertArrayEquals(c,r);
  }
  //Test printDice()
  @Test
  public void testPrintDice1() {
        int[] g = {1,2,3,4,5};
        String result = student.printDice(g);
        String wspace = "1 2 3 4 5 ";
        String wospace = "1 2 3 4 5";

        assertTrue("printDice output did not match '1 2 3 4 5 ' or '1 2 3 4 5'",result.equals(wspace)||result.equals(wospace));
  }
  @Test
  public void testPrintDice2(){
    int[] g = {5,4,3,2,1};
    String result = student.printDice(g);
    String wspace = "5 4 3 2 1 ";
    String wospace = "5 4 3 2 1";

    assertTrue("printDice output did not match '5 4 3 2 1 ' or '5 4 3 2 1'", result.equals(wspace)||result.equals(wospace));
  }
  //Test kinds()
  @Test
  public void testKinds1(){
    int[] g = {1,1,3,5,6};
    int[] correct = {2,0,1,0,1,1};
    int[] result = student.kinds(g);

    assertArrayEquals(correct,result);
  }
  @Test
  public void testKinds2(){
    int[] g = {2,3,4,4,6};
    int[] correct = {0,1,1,2,0,1};
    int[] result = student.kinds(g);

    assertArrayEquals(correct,result);
  }
  //Test printScore
  @Test
  public void testPrintScore1(){
    //6 possible Yahtzees
    int[][] dice = {{4,4,4,4,4},{6,6,6,6,6},{5,5,5,5,5},{3,3,3,3,3},{2,2,2,2,2},{1,1,1,1,1}};
    String c = "Yahtzee";
    String[] correct = new String[dice.length];
    for (int i = 0; i<dice.length; i++){
      correct[i] = c;
    }
    String[] results = new String[dice.length];
    for (int i = 0; i < dice.length; i++){
        results[i] = student.printScore(dice[i]);
      }
    assertArrayEquals(correct,results);
  }
  @Test
  public void testPrintScore2(){
    //2 possible Large Straights
    int[][] dice = {{4,5,6,3,2},{5,2,4,1,3}};
    String c = "Large Straight";
    String[] correct = new String[dice.length];
    for (int i = 0; i<dice.length; i++){
      correct[i] = c;
    }
    String[] results = new String[dice.length];
    for (int i = 0; i < dice.length; i++){
        results[i] = student.printScore(dice[i]);
      }
    assertArrayEquals(correct,results);
  }
  @Test
  public void testPrintScore3(){
    //14 possible Small Straights
    int[][] dice = {
      {1,2,3,4,6},{1,2,3,4,1},{1,2,3,4,2},{1,2,3,4,3},{1,2,3,4,4},
      {2,3,4,5,2},{2,3,4,5,3},{2,3,4,5,4},{2,3,4,5,5},
      {3,4,5,6,6},{3,4,5,6,1},{3,4,5,6,3},{3,4,5,6,4},{3,4,5,6,5}
    };
    String c = "Small Straight";
    String[] correct = new String[dice.length];
    for (int i = 0; i<dice.length; i++){
      correct[i] = c;
    }
    String[] results = new String[dice.length];
    for (int i = 0; i < dice.length; i++){
        results[i] = student.printScore(dice[i]);
      }
    assertArrayEquals(correct,results);
  }
  @Test
  public void testPrintScore4(){
    //testing 2 of the possible 4 o' Kinds
    int[][] dice = {{4,4,3,4,4},{3,3,3,5,3}};
    String c = "Four of a Kind";
    String[] correct = new String[dice.length];
    for (int i = 0; i<dice.length; i++){
      correct[i] = c;
    }
    String[] results = new String[dice.length];
    for (int i = 0; i < dice.length; i++){
        results[i] = student.printScore(dice[i]);
      }
    assertArrayEquals(correct,results);
  }
  @Test
  public void testPrintScore5(){
    //testing 3 of the possible full houses
    int[][] dice = {{2,2,3,3,3},{4,4,4,6,6},{1,4,1,4,1}};
    String c = "Full House";
    String[] correct = new String[dice.length];
    for (int i = 0; i<dice.length; i++){
      correct[i] = c;
    }
    String[] results = new String[dice.length];
    for (int i = 0; i < dice.length; i++){
        results[i] = student.printScore(dice[i]);
      }
    assertArrayEquals(correct,results);
  }
  @Test
  public void testPrintScore6(){
    //testing 2 of the possible 3 o' kinds
    int[][] dice = {{4,4,4,1,2},{1,3,5,3,3}};
    String c = "Three of a Kind";
    String[] correct = new String[dice.length];
    for (int i = 0; i<dice.length; i++){
      correct[i] = c;
    }
    String[] results = new String[dice.length];
    for (int i = 0; i < dice.length; i++){
        results[i] = student.printScore(dice[i]);
      }
    assertArrayEquals(correct,results);
  }
  @Test
  public void testPrintScore7(){
    //testing 2 of the possible no score
    int[][] dice = {{1,1,4,5,3},{6,5,3,5,6}};
    String c = "You did not score anything noteworthy.";
    String[] correct = new String[dice.length];
    for (int i = 0; i<dice.length; i++){
      correct[i] = c;
    }
    String[] results = new String[dice.length];
    for (int i = 0; i < dice.length; i++){
        results[i] = student.printScore(dice[i]);
      }
    assertArrayEquals(correct,results);
  }
}